import { compose, withState, withHandlers, withProps } from 'recompose';

import Autocomplete from '../Components/Autocomplete';

const getItems = () =>
    new Promise(resolve => {
        setTimeout(() => {
            // переведёт промис в состояние fulfilled с результатом "result"
            resolve({
                result: [{ name: 'BCT', value: '12.35' }, { name: 'ETN', value: '11.34' }],
            });
        }, 1000);
    });

const defaultProps = compose(
    withState('value', 'setValue', 'default'),
    withHandlers({
        onChange: ({ setValue }) => ({ target }) => {
            setValue(target.innerText);
        },
    }),
    withProps(() => ({
        getItems,
        className: 'hiClassName',
    }))
);

export default defaultProps(Autocomplete);
