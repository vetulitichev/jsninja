import React from 'react';

import { storiesOf } from '@storybook/react';

// Stories
import Autocomplete from './Story@1.0.0';

const Item = ({ name, value }) => (
    <p>
        {name}({value})
    </p>
); // eslint-disable-line

storiesOf('JavaScript Ninja', module).add('Autocomplete', () => (
    <Autocomplete>
        <Item item />
    </Autocomplete>
)); // eslint-disable-line
