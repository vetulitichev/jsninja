import { compose, withHandlers, mapProps, withState } from 'recompose';
import seapig from 'seapig';
import Autocomplete from './Autocomplete';

const withAutocomplete = compose(
    withState('isSelected', 'setIsSeleted', false),
    withState('dropDownList', 'setDropDownList', []),
    withHandlers({
        onValueChange: ({ setValue, getItems, setDropDownList, setIsSeleted }) => async ({
            target,
        }) => {
            if (
                this.promice !== undefined &&
                this.promice !== 'done' &&
                this.promice.abort &&
                typeof this.promice.abort === 'function'
            ) {
                this.promice.abort();
            }

            setValue(target.value);
            setIsSeleted(false);
            this.promice = getItems().then(res => {
                setDropDownList(res.result);
                this.promice = 'done';
            });
        },
        handleClickOutside: ({ setValue, setDropDownList, isSelected }) => () => {
            if (isSelected) {
                setDropDownList([]);
            } else {
                setDropDownList([]);
                setValue('');
            }
        },
        onValueSelect: ({ setIsSeleted, onChange }) => event => {
            setIsSeleted(true);
            onChange(event);
        },
    }),
    mapProps(props => {
        const { itemChildren } = seapig(props.children, {
            item: {
                min: 1,
                max: 1,
            },
        });
        const {
            setValue,
            getItems,
            setDropDownList,
            setIsSeleted,
            isSelected,
            children,
            onChange,
            ...rest
        } = props;
        return { component: itemChildren[0], ...rest };
    })
);

export default withAutocomplete(Autocomplete);
