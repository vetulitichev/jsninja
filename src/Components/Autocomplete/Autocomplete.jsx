import React from 'react';
import { string, element, func, object, array, bool, number, arrayOf } from 'prop-types';
import onClickOutside from 'react-onclickoutside';
import v4 from 'uuid/v4';

// Style
import './style.css';

const Autocomplete = ({
    value,
    onValueChange,
    onValueSelect,
    dropDownList,
    className,
    component,
}) => (
    <div className={className}>
        <input id="autocomplete" type="text" onChange={onValueChange} value={value} />
        <div id="autocomplete_result" style={{ display: 'block' }}>
            {dropDownList.map(itemValues => {
                const test = React.cloneElement(component, { ...itemValues });
                return (
                    <div key={v4()} onClick={onValueSelect}>
                        {test}
                    </div>
                );
            })}
        </div>
    </div>
);

Autocomplete.propTypes = {
    value: string,
    component: element.isRequired,
    onValueChange: func,
    onValueSelect: func,
    dropDownList: arrayOf(string, number, object, array, bool),
};
Autocomplete.defaultProps = {
    value: '',
    dropDownList: [],
    onValueChange: () => {},
    onValueSelect: () => {},
};

export default onClickOutside(Autocomplete);
